$('.slider').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<button type="button" class="slick-prev"><img src="./images/left-arrow.png" alt=""></button>',
    nextArrow: '<button type="button" class="slick-next"><img src="./images/right-arrow.png" alt=""></button>',
    responsive: [
      {
        breakpoint: 1024,
        settings: {
        }
      },
      {
        breakpoint: 600,
        settings: {
        }
      },
      {
        breakpoint: 480,
        settings: {
        }
      }
    ]
  });

  function managedServices(){
    if( document.querySelector('.step2') && document.querySelector('.managed-services')){
        var step2 =  document.querySelector('.step2');    
        var managedServices = document.querySelector('.managed-services');
        var sirclesArray = Array.from(managedServices.querySelectorAll('.slider-sircle'));
        var windowHeight = window.innerHeight;
        var blocPosition = step2.offsetTop;
        var step2Height = step2.offsetHeight;
        var distance;
        var color;
        window.addEventListener('scroll', function(){
            if((window.scrollY+ windowHeight/2) > (blocPosition + step2Height/2) ){
                sirclesArray.forEach((item, index)=>{
                    distance = item.dataset.distance;
                    color = item.dataset.color;
                    item.style.left = distance;
                    item.style.background = color;

                });
            }
        });
    }    
  }
  var mobile_menu = $('.js_mobile-menu');
  function mobile_menu_toggle(){
    var menu_button = $('.menu-button');
    var close_btn = $('.close_btn');
    menu_button.on('click',function(){
      mobile_menu.slideDown();
    });

    close_btn.on('click', function(){
      mobile_menu.slideUp();
    });

  }

  function footerAdaptation(){
    $(window).resize(function(){
      if (window.innerWidth < 479) {
        $(".footer .column ul").slideUp();
      } else {
        $(".footer .column ul").slideDown();
      }
    });
    var flag = true;
    $(".footer .column h4").on("click", function() {      
      if(window.innerWidth <767 && flag){
        flag = false;
        $(this).closest('.column').find('ul').slideToggle();
        setTimeout(function(){
          flag = true;
        }, 500);
        
      }      
    });
  }


$(document).ready(function(){
    footerAdaptation();
    managedServices();
    mobile_menu_toggle();
    if(window.innerWidth > 467){   
      mobile_menu.slideDown(1);      
      $(".footer .column ul").slideDown();
    }else{
      mobile_menu.slideUp(1); 
      $(".footer .column ul").slideUp();    
    }

});

$(window).load(function(){

});

$(window).resize(function(){
  
  if(window.innerWidth > 767){   
    mobile_menu.slideDown(1);
  }else{
    mobile_menu.slideUp(1);
  }

 
});